import React, { PropTypes, Component} from 'react'

export default class Page extends Component {
  onYearBtnClick(e) {
    this.props.getPhotos(+e.target.textContent)
  }

  render () {
    const { year, photos, fetching, erros } = this.props
    const years = [2016, 2015, 2014, 2013, 2012, 2011, 2010]

    return <div className='ib page'>
      <p>
        { years.map((item, index) => {
          <button className='btn' onClick={::this.onYearBtnClick}>
            {item}
          </button>
        })}
      </p>

      {/* <p>
        <button className='btn' onClick={::this.onYearBtnClick}>2016</button>{' '}
        <button className='btn' onClick={::this.onYearBtnClick}>2015</button>{' '}
        <button className='btn' onClick={::this.onYearBtnClick}>2014</button>
      </p> */}
      <h3>{year} год</h3>
      {
        fetching ?
        <p>Загрузка...</p>
        :
        photos.map((entry, index) =>
          <div key={index}>
            <p><img src={entry.src} /></p>
            <p>{entry.likes.count} ❤</p>
          </div>
        )
      }
    </div>
  }
}

Page.propTypes = {
  year: PropTypes.number.isRequired,
  photos: PropTypes.array.isRequired,
  getPhotos: PropTypes.func.isRequired,
  error: PropTypes.string.isRequired
}
